﻿using activity3_part1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace activity3_part1.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        [HttpGet]
        public ActionResult Index()
        {

            var customer = new CustomerModel(1, "William", 23);
            return View("Customer", customer);
        }
    }
}